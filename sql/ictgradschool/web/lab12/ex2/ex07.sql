
-- Answers to exercise 7 questions
DROP TABLE IF EXISTS player;
CREATE TABLE IF NOT EXISTS player(
playerid INT NOT NULL,
name VARCHAR(1000),
age INT NOT NULL,
nationality VARCHAR(500),
PRIMARY KEY(playerid)
);

DROP TABLE IF EXISTS sportsteam;
CREATE TABLE IF NOT EXISTS sportsteam (
id INT NOT NULL,
name VARCHAR(1000),
captain INT NOT NULL,
homecity VARCHAR(100),
points INT NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (captain) REFERENCES player(playerid));



INSERT INTO player (playerid, name, age, nationality) VALUES
(11111, "James", 50, "US"),
(22222, "Milsep", 25, "KR"),
(33333, "Derrick Rose", 29, "INA");

INSERT INTO sportsteam (id, name, captain, homecity, points) VALUES
(1111, "Lakers", 11111, "LA", 3),
(2222, "Nuggets", 22222, "Denver", 10),
(3333, "Wolves", 33333, "Minnesota", 39);















