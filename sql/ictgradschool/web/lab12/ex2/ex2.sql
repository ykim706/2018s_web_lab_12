-- Answers to exercise 2 questions
SELECT a.fname, a.lname
FROM unidb_students AS a,
unidb_attend AS b
WHERE a.id = b.id AND b.dept = 'comp' AND b.num = '219';


SELECT a.fname, a.lname
FROM unidb_students AS a,
unidb_courses AS b
WHERE a.id = b.rep_id AND NOT a.country = 'NZ';


SELECT a.office
FROM unidb_lecturers AS a,
unidb_teach AS b
WHERE a.staff_no = b.staff_no AND b.num = '219';

SELECT DISTINCT a.fname, a.lname
FROM unidb_students AS a,
unidb_lecturers AS b,
unidb_teach AS c,
unidb_attend AS d
WHERE b.fname = 'Te Taka'
AND b.staff_no = c.staff_no
AND c.dept = d.dept AND c.num = d.num
AND d.id = a.id;

SELECT a.fname, a.lname, b.fname, b.lname
FROM unidb_students AS a,
unidb_students AS b
WHERE a.mentor = b.id;

SELECT a.fname, a.lname
FROM unidb_lecturers AS a
WHERE a.office LIKE 'G%'
UNION
SELECT b.fname, b.lname
FROM unidb_students AS b
WHERE b.country NOT LIKE 'NZ';


SELECT a.fname, a.lname, b.fname, b.lname
FROM unidb_lecturers AS a,
unidb_students AS b,
unidb_courses AS c
WHERE c.dept = 'comp' AND c.num = '219'
AND c.coord_no = a.staff_no AND c.rep_id = b.id;





