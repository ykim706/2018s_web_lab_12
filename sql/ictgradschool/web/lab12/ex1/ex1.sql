-- Answers to exercise 1 questions
SELECT dept
FROM unidb_courses;

SELECT semester
FROM unidb_attend;

SELECT DISTINCT a.rep_id
FROM unidb_courses AS a,
unidb_attend AS b
WHERE a.rep_id = b.id;

SELECT b.fname, b.lname, b.country
FROM unidb_students AS b
ORDER BY fname;

SELECT b.fname, b.lname, b.mentor
FROM unidb_students AS b
ORDER BY mentor;

SELECT a.fname, a.lname
FROM unidb_lecturers AS a
ORDER BY office;

SELECT a.fname, a.lname
FROM unidb_lecturers AS a
WHERE staff_no > 500;

SELECT a.fname, a.lname
FROM unidb_students AS a
WHERE id > 1668 AND id < 1824;

SELECT a.fname, a.lname
FROM unidb_students AS a
WHERE country = "NZ" OR country = "AU" OR country = "US";

SELECT a.fname, a.lname
FROM unidb_lecturers AS a
WHERE office LIKE 'G%';

SELECT a.dept
FROM unidb_courses AS a
WHERE dept NOT LIKE 'comp';

SELECT a.fname, a.lname
FROM unidb_students AS a
WHERE country = "FR" OR country = "MX";








